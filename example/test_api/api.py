import dataclasses
from typing import List

from django_fastapi import get_default_api

api = get_default_api()


@dataclasses.dataclass
class HelloWorld:
    hello: str
    foo: str
    another_resource: str
    docs: List[str] = dataclasses.field(
        default_factory=lambda: ["/docs/", "/redoc/"])


@dataclasses.dataclass
class AnotherResource:
    another: str


@api.get("/", response_model=HelloWorld)
def serve_root():
    return HelloWorld(hello="world", foo="bar", another_resource="/another/")


@api.get("/another/", response_model=AnotherResource)
def another_resource():
    return AnotherResource(another="resource")
