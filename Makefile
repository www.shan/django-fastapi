.PHONY: package upload

env:
	python3 -m venv env
	pip install wheel twine

package: env
	@rm -rf dist
	@mkdir dist
	@source env/bin/activate && python setup.py clean sdist bdist_wheel

upload: package
	twine upload --verbose --skip-existing dist/*
